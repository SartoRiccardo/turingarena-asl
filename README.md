# turingarena-asl

Here you can find all of the TuringArena problems we made and are making.
The ones in the `tests` folder are beginner problems to see how TuringArena handles certain types of data.
The ones in the `finished` folder are meant to be finished products.