import random
import math
from turingarena import *

def main():

	gotten_all = True
	all_correct = True

	for _ in range(10):
		num = random.randint(0, 250)

		try:
			with run_algorithm(submission.source) as process:
				num_div = process.functions.get_number_of_divisors(num)
				process.procedures.get_divisors(num)
				divisors = [process.functions.get_divisor(i) for i in range(num_div)]
	
		except AlgorithmError as e:
			pass

		if has_all_divisors(divisors, num):
			print("(correct)", end='')
		else:
			print("(divisor(s) missing)", end='')
			gotten_all = False

		if all_divisors_are_correct(divisors, num):
			print("(correct)", end=' ')
		else:
			print("(divisors(s) wrong)", end=' ')
			all_correct = False

		if num < 100:
			print(f"0{num}", end='')	
		else:
			print(f"{num}", end='')
		print(f"'s divisors are: {divisors}")

	evaluation.data(dict(goals=[dict(gotten_all_divisors=gotten_all), dict(all_divisors_correct=all_correct)]))



def has_all_divisors(divisors, num):
	correct_divisors = get_correct_divisors(num)

	for i in range(len(correct_divisors)):
		appears = False

		for j in range(len(divisors)):
			if correct_divisors[i] == divisors[j]:
				appears = True
				break;

		if not appears:
			return False

	return True


def all_divisors_are_correct(divisors, num):
	for i in range(len(divisors)):
		if not num%divisors[i] == 0:
			return False

	return True


def get_correct_divisors(num):
	divisors = []
	
	for i in range(1, math.floor(num/2)+1):
		if num%i == 0:
			divisors.append(i);

	if not num == 0:
		divisors.append(num)

	return divisors


main()
