int *divisors;

int get_number_of_divisors(int num) {

	int number_of_divisors = 1;

	for(int i = 1; i <= num/2; i++) {

		if(num%i == 0) number_of_divisors++;

	}

	return number_of_divisors;

}

void get_divisors(int num) {

	divisors = new int[get_number_of_divisors(num)];
	
	int divisors_index = 0;
	for(int i = 1; i <= num; i++) {

		if (num%i == 0) {
			divisors[divisors_index] = i;
			divisors_index++;
		}

	}

}

int get_divisor(int i) {

	return divisors[i];

}
