// Constant declarations
#define COLUMNS 7
#define CHECKER_WAS_PLACED 0
#define ERROR_COLUMN_OUT_OF_RANGE 1
#define ERROR_COLUMN_FULL 2
#define ERROR_ALREADY_MADE_MOVE 3
#define EMPTY 0
#define YOU 1
#define ENEMY 2


void play_turn(int **grid, int place_checker(int column)) {

	int column = -1;
	int result;
	do {
		column++;
		result = place_checker(column);
	} while(result != CHECKER_WAS_PLACED);

}
