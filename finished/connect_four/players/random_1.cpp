#include <cstdlib>
#include <ctime>

// Constant declarations
#define COLUMNS 7

#define CHECKER_WAS_PLACED 0
#define ERROR_COLUMN_OUT_OF_RANGE 1
#define ERROR_COLUMN_FULL 2
#define ERROR_ALREADY_MADE_MOVE 3

#define EMPTY 0
#define YOU 1
#define ENEMY 2


// initialize random number generator
__attribute__((constructor)) void init() {
    srand(time(nullptr));
}

void play_turn(int **grid, int place_checker(int column)) {
	
	int result;
	do result = place_checker((rand()%COLUMNS + rand()%COLUMNS)%COLUMNS);
	while(result != CHECKER_WAS_PLACED);

}
