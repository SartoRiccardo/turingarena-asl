from turingarena import *
import sys
#from termcolor import colored

ROWS = 6
COLUMNS = 7

CHECKER_WAS_PLACED = 0
ERROR_COLUMN_OUT_OF_RANGE = 1
ERROR_COLUMN_FULL = 2
ERROR_ALREADY_MADE_MOVE = 3

EMPTY, DRAW = 0, 0
YOU, WIN = 1, 1
ENEMY = 2

board_pieces = {
	EMPTY:	'*',
	YOU:	'O',#colored("O", 'red'),
	ENEMY:	'X'#colored("X", 'yellow')
}

def get_empty_board(rows, columns):
	board = []
	
	for i in range(rows):
		row = []
		for j in range(columns):
			row.append(EMPTY)
		board.append(row)

	return board

def is_out_of_bounds(x, y):
	if x < 0 or y < 0 or x >= COLUMNS or y >= ROWS:
		return True
	
	return False

def switch_point_of_view():
	for i in range(len(board)):
		for j in range(len(board[i])):
			if board[i][j] == YOU:
				board[i][j] = ENEMY
			elif board[i][j] == ENEMY:
				board[i][j] = YOU

def check_if_won():

	global last_x, last_y

	connected = 0
	for i in range(COLUMNS):
		if board[last_y][i] == YOU:
			connected += 1
			if connected == 4:
				return WIN
		else:
			connected = 0

	connected = 0
	for i in range(ROWS):
		if board[i][last_x] == YOU:
			connected += 1
			if connected == 4:
				return WIN
		else:
			connected = 0

	x, y = last_x, last_y
	while x > 0 and y < ROWS-1:
		x -= 1
		y += 1

	connected = 0
	while y >= 0 and x < COLUMNS:
		if board[y][x] == YOU:
			connected += 1
			if connected == 4:
				return WIN
		else:
			connected = 0

		x += 1
		y -= 1

	x, y = last_x, last_y
	while x > 0 and y > 0:
		x -= 1
		y -= 1

	connected = 0
	while y < ROWS and x < COLUMNS:
		if board[y][x] == YOU:
			connected += 1
			if connected == 4:
				return WIN
		else:
			connected = 0

		x += 1
		y += 1


	for i in range(ROWS):
		for j in range(COLUMNS):
			if board[i][j] == EMPTY:
				return -1

	return DRAW

def print_board():
	for i in range(len(board)):
		for j in range(len(board[i])):
			print(board_pieces[board[i][j]], end=' ')
		print()


wins = [0, 0]
MATCHES = 100
for match in range(MATCHES):
	board = get_empty_board(ROWS, COLUMNS)

	with run_algorithm(submission.player1) as p1, run_algorithm(submission.player2) as p2:

		def place_checker(column):
			global made_move, last_x, last_y
			if made_move:
				return ERROR_ALREADY_MADE_MOVE

			if column < 0 or column >= COLUMNS:
				return ERROR_COLUMN_OUT_OF_RANGE

			found_empty_spot = False
			for i in range(ROWS):
				if board[ROWS-i-1][column] == EMPTY:
					found_empty_spot = True
					board[ROWS-i-1][column] = YOU
					last_x, last_y = column, ROWS-i-1
					break

			if not found_empty_spot:
				return ERROR_COLUMN_FULL

			made_move = True
			return CHECKER_WAS_PLACED

		players = (p1, p2)
		current_player = match%2
		outcome = -1
		while outcome < 0:
			current_player = (current_player+1)%2
			switch_point_of_view()

			global made_move, last_x, last_y
			made_move = False
			players[current_player].procedures.play_turn(board, callbacks=[place_checker])

			outcome = check_if_won()


	if current_player == 1:
		switch_point_of_view()
	
	print_board()
	if outcome == DRAW:
		print("It's a draw!")
	else:
		print(f"Player {current_player+1} wins the game!")
		wins[current_player] += 1
	print()

print(f"Final score: {wins[0]} - {wins[1]}.", end=' ')
if wins[0] > wins[1]:
	print("Player 1 wins the match!")
elif wins[0] < wins[1]:
	print("Player 2 wins the match!")
else:
	print("It's a draw!")







			
