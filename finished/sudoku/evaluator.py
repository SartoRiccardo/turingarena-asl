from turingarena import *
try:
	from termcolor import colored
	tc = True
except ImportError:
	print("Install the termcolor module for a better experience.")
	tc = False

def main():
	global ans, case, solution
	all_passed = True

	raw_cases = open("cases/cases.txt", "r").readlines()
	raw_solutions = open("cases/solutions.txt", "r").readlines()
	raw_cases = [x.strip() for x in raw_cases]
	raw_solutions = [x.strip() for x in raw_solutions]

	for number_of_case in range(int(raw_cases[0])):
		case = get_sudoku(number_of_case, raw_cases)
		solution = get_sudoku(number_of_case, raw_solutions)

		with run_algorithm(submission.source) as process:
			process.procedures.start(len(case), len(case[0]))
			process.procedures.fill(case)
			ans = [[process.functions.get_element(i, j) for j in range(len(case[i]))] for i in range(len(case))]
		
		if solution == ans:
			print("Correct!")
		else:
			print("Wrong!")
			all_passed = False
		print_sudoku()
		print()

	evaluation.data(dict(goals=[dict(all_passed=all_passed)]))
	

def print_sudoku():
	colors = {
		'input': 'blue',
		'wrong': 'red',
		'right': 'green'
	}

	global ans, case, solution
	for i in range(len(ans)):
		for j in range(len(ans[i])):
			if not case[i][j] == 0:
				print(ans[i][j] if not tc else colored(ans[i][j], colors['input']), end=' ')
			elif ans[i][j] == solution[i][j]:
				print(ans[i][j] if not tc else colored(ans[i][j], colors['right']), end=' ')
			else:
				print(ans[i][j] if not tc else colored(ans[i][j], colors['wrong']), end=' ')
		print()


def get_sudoku(number_of_case, sudoku_list):
	sudoku = []
	line_count = 2
	
	for i in range(number_of_case):
		line_count += int(sudoku_list[line_count].split('x')[0]) + 2

	for i in range(int(sudoku_list[line_count].split("x")[0])):
		sudoku.append(sudoku_list[line_count + i + 1].split(' '))
		sudoku[i] = [int(sudoku[i][j]) for j in range(len(sudoku[i]))]

	return sudoku


main()

















