import math

def start(x, y):
	pass

# Only works for simple 9x9 sudokus, goes on endless loop during the 6th example
def fill(case):
	def isFull():
		for row in sudoku:
			for num in row:
				if num == 0:
					return False
		return True

	def getNumberOfSquares():
		return int(math.sqrt(len(sudoku)) * math.sqrt(len(sudoku[0])))

	def getForbiddenIndexes(num):
		for i in range(len(sudoku)):
			row = sudoku[i]
			if num in row:
				forbiddenY.append(i)
				forbiddenX.append(row.index(num))

	def getSquareIndexes(square_num):
		square_dim = [int(math.sqrt(len(sudoku))), int(math.sqrt(len(sudoku[0])))]
		squarei, squarej = 0, 0
		for i in range(square_num):
			squarei += square_dim[0]
			if squarei%len(sudoku) == 0:
				squarei = 0
				squarej += square_dim[1]

		return squarei, squarej
			

	def getSquare(squarei, squarej):
		square_dim = [int(math.sqrt(len(sudoku))), int(math.sqrt(len(sudoku[0])))]
		square = []
		for i in range(square_dim[0]):
			square_row = []
			for j in range(square_dim[1]):
				square_row.append(sudoku[squarei+i][squarej+j])
			square.append(square_row)

		return square

	def squareHasNumber(num, square):
		for row in square:
			for n in row:
				if n == num:
					return True
		return False

	def isForbidden(i, j):
		return i in forbiddenY or j in forbiddenX

	def putNumberInSquare(num, squarei, squarej):
		square_dim = [int(math.sqrt(len(sudoku))), int(math.sqrt(len(sudoku[0])))]
		numi, numj = -1, -1

		square = getSquare(squarei, squarej)
		for i in range(square_dim[0]):
			for j in range(square_dim[1]):
				if square[i][j] == 0 and not numi == -1 and not isForbidden(squarei+i, squarej+j):
					return
				elif square[i][j] == 0 and not isForbidden(squarei+i, squarej+j):
					numi = i
					numj = j
		
		if numi == -1:
			return
		sudoku[squarei+numi][squarej+numj] = num

	global sudoku
	sudoku = case
	forbiddenX, forbiddenY = [], []

	while not isFull():
		for num in range(1, getNumberOfSquares()+1):

			for i in range(getNumberOfSquares()):
				getForbiddenIndexes(num)

				squarei, squarej = getSquareIndexes(i)

				if not squareHasNumber(num, getSquare(squarei, squarej)):
					putNumberInSquare(num, squarei, squarej)

				forbiddenX, forbiddenY = [], []
		

def get_element(i, j):
	return sudoku[i][j]

def printSudoku():
	for row in sudoku:
		for num in row:
			print(num, end=' ')
		print()
	print()
