/* To test how the evaluator would react to a sloppy bot */

#define FIELD_START_INDEX 1
#define FIELD_END_INDEX 10
#define FOUR_PANEL_SHIPS 1
#define THREE_PANEL_SHIPS 2
#define TWO_PANEL_SHIPS 3
#define ONE_PANEL_SHIPS 4
#define SHIP_WAS_PLACED_CORRECTLY 0
#define ERROR_IN_PLACING_SHIP 1
#define ALREADY_GUESSED -1
#define MISS 0
#define HIT 1
#define HIT_AND_SANK 2

void start(int n) {
    // nothing
}

void place_all_ships(int place(int ship_len, int x, int y, int is_horizontal)) {
    // nothing
}

void play(int guess(int x, int y)) {
    // nothing
}
