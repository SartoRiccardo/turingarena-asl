#include <stdlib.h>

#define FIELD_START_INDEX 1
#define FIELD_END_INDEX 10

#define FOUR_PANEL_SHIPS 1
#define THREE_PANEL_SHIPS 2
#define TWO_PANEL_SHIPS 3
#define ONE_PANEL_SHIPS 4

#define SHIP_WAS_PLACED_CORRECTLY 0
#define ERROR_IN_PLACING_SHIP 1

#define ALREADY_GUESSED -1
#define MISS 0
#define HIT 1
#define HIT_AND_SANK 2

void place_ship_type(int ship_type, int ship_num, int place(int ship_len, int x, int y, int is_horizontal)) {

	for(int i = 0; i < ship_num; i++) {
		int placed_correctly;

		do {
			placed_correctly = place(ship_type, rand()%FIELD_END_INDEX+FIELD_START_INDEX, rand()%FIELD_END_INDEX+FIELD_START_INDEX, rand()%2);
		} while(placed_correctly != SHIP_WAS_PLACED_CORRECTLY);
	}

}

void place_all_ships(int place(int ship_len, int x, int y, int is_horizontal)) {

	place_ship_type(4, FOUR_PANEL_SHIPS, place);
	place_ship_type(3, THREE_PANEL_SHIPS, place);
	place_ship_type(2, TWO_PANEL_SHIPS, place);
	place_ship_type(1, ONE_PANEL_SHIPS, place);

}

void start(int n) {}

void play(int guess(int x, int y)) {

	for(int i = 1; i <= 10; i++) {
		for(int j = 1; j <= 10; j++)
			guess(j, i);
	}

}
