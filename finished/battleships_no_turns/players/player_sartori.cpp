#include <stdlib.h>
#include <chrono>

#define FIELD_START_INDEX 1
#define FIELD_END_INDEX 10

#define FOUR_PANEL_SHIPS 1
#define THREE_PANEL_SHIPS 2
#define TWO_PANEL_SHIPS 3
#define ONE_PANEL_SHIPS 4

#define SHIP_WAS_PLACED_CORRECTLY 0
#define ERROR_IN_PLACING_SHIP 1

#define ALREADY_GUESSED -1
#define MISS 0
#define HIT 1
#define HIT_AND_SANK 2


int total_ship_len;

void start(int n) {
	srand(1300);
}

void place_ship_type(int ship_len, int ship_num, int place(int ship_len, int x, int y, int is_horizontal)) {

	for(int i = 0; i < ship_num; i++) {
		int placed_correctly;

		do placed_correctly = place(ship_len, rand()%FIELD_END_INDEX+FIELD_START_INDEX, rand()%FIELD_END_INDEX+FIELD_START_INDEX, rand()%2);
		while(placed_correctly != SHIP_WAS_PLACED_CORRECTLY);
		
		total_ship_len += ship_len;
	}

}

void place_all_ships(int place(int ship_len, int x, int y, int is_horizontal)) {

	total_ship_len = 0;

	place_ship_type(4, FOUR_PANEL_SHIPS, place);
	place_ship_type(3, THREE_PANEL_SHIPS, place);
	place_ship_type(2, TWO_PANEL_SHIPS, place);
	place_ship_type(1, ONE_PANEL_SHIPS, place);

}

bool is_in_bounds(int number) {
	return FIELD_END_INDEX >= number && number >= FIELD_START_INDEX;
}

void copy_array(int *from, int *to, int len) {
	for (int i = 0; i < len; i++)
		to[i] = from[i];
}

void play(int guess(int x, int y)) {
	int hits = 0;
	bool is_horizontal;

	while(hits < total_ship_len) {
		int coords[2] = {rand()%10+1, rand()%10+1};
		switch(guess(coords[0], coords[1])) {

			case ALREADY_GUESSED: case MISS: break;
	
			case HIT:
			{
				hits++;
				bool sank = false;

				int ship_start[2] = {coords[0], coords[1]};

				int relative_coords[4][2] = {{0, 1}, {-1, 0}, {1, 0}, {0, -1}};
				int direction;
				for(int i = 0; i < 4; i++) {
					if(is_in_bounds(ship_start[0]+relative_coords[i][0]) && is_in_bounds(ship_start[1]+relative_coords[i][1])) {
						int results = guess(ship_start[0]+relative_coords[i][0], ship_start[1]+relative_coords[i][1]);
						if(results > MISS) {
							hits++;
							if(results == HIT_AND_SANK) sank = true;
							is_horizontal = relative_coords[i][0] != 0;
							direction = relative_coords[i][0] + relative_coords[i][1];
							break;
						}
					}
				}

				int offset = direction*2;
				while(!sank) {
					if(!is_in_bounds(ship_start[0]+offset) && is_horizontal || !is_in_bounds(ship_start[1]+offset) && !is_horizontal) {
						direction *= -1;
						offset = direction;
					}

					if(is_horizontal) copy_array(new int[2]{ship_start[0]+offset, ship_start[1]}, coords, 2);
					else copy_array(new int[2]{ship_start[0], ship_start[1]+offset}, coords, 2);

					switch(guess(coords[0], coords[1])) {

						case ALREADY_GUESSED: case MISS: direction *= -1; offset = direction; break;

						case HIT: hits++; offset += direction; break;

						case HIT_AND_SANK: hits++; sank = true;
	
					}

				}
				break;
			}

			case HIT_AND_SANK: hits++;

		}
	}

}
