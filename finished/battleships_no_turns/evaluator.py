import random
from turingarena import *

def reset(matrix, value):
	global field_size
	field_size = 10
	for i in range(len(matrix)):
		matrix.pop(i)
	
	for i in range(field_size):
		row = [value for j in range(field_size)]
		matrix.append(row)
		

def print_grid(grid):
	for i in range(len(grid)):
		print("{:02d}".format(field_size - i), end=' ')
		for j in range(len(grid[i])):
			print(board_pieces[grid[i][j]], end=' ')
		print()
	print()

def print_grids():
	def trim_plays(p1_plays, p2_plays):
		if len(p1_plays) >= len(p2_plays):
			for i in range(len(p1_plays)-len(p2_plays)+1):
				p2_plays.pop(-1)
		else:
			for i in range(len(p2_plays)-len(p1_plays)+1):
				p1_plays.pop(-1)

	def get_char_if_called(x, y):
		if grid[x][y] == 0:
			return grid[x][y]
		elif grid[x][y] == -1 or grid[x][y] == 2:
			if [x, y] in p_plays:
				return grid[x][y]
			elif grid[x][y] == 2:
				return 1
			else:
				return 0

	trim_plays(plays[first_player], plays[(first_player+1)%2])

	for i in range(field_size):
		for p in range(2):
			grid = fields[p]
			p_plays = plays[p]

			print("{:02d}".format(field_size - i), end=' ')
			for j in range(field_size):
				print(board_pieces[get_char_if_called(i, j)], end=' ')
			print("\t\t", end='')

		print()

	for p in range(2):
		print("  ", end='')
		for j in range(field_size):
			print(f" {j+1}", end='')
		print("\b\t\t", end='')

	print()


board_pieces = {
	-1:"±",
	0:"∼",
	1:"#",
	2:"/",
}

with run_algorithm(submission.player1) as p1, run_algorithm(submission.player2) as p2:

	def is_in_bounds(num):
		return num >= 0 and num < field_size

	def place(ship_len, x, y, is_horizontal):
		def is_a_suitable_place():
			grid = fields[current_player]

			if not grid[field_size-y][x-1] == 0:
				return False

			try:
				for i in range(ship_len):
					ship_coords = [x-1, field_size-y]
					if is_horizontal:
						ship_coords[0] += i
					else:
						ship_coords[1] -= i

					if is_horizontal and not is_in_bounds(ship_coords[0]) or not is_horizontal and is_in_bounds(ship_coords[1]):
						return False

					rel_coords = [[0, 1], [-1, 0], [1, 0], [0, -1]]
					for coords in rel_coords:
						try:
							if not grid[ship_coords[1]+coords[1]][ship_coords[0]+coords[0]] == 0:
								return False
						except IndexError:
							pass

				return True

			except IndexError:
				return False

		grid = fields[current_player]

		if not is_a_suitable_place():
			return 1

		for i in range(ship_len):
			if is_horizontal == 1:
				grid[field_size-y][x-1+i] = 1
			else:
				grid[field_size-y-i][x-1] = 1

		ships[current_player].append(ship_len)

		return 0


	def guess(x, y):
		def is_placed_horizontally(grid, x, y):
			rel_coords = [[0, -1],[-1, 0], [1, 0], [0, 1]]

			for coord in rel_coords:
				try:
					if grid[field_size-y+coord[1]][x-1+coord[0]] > 0:
						return not coord[0] == 0
						break
				except IndexError:
					pass

		enemy_grid = fields[(current_player+1)%2]

		if enemy_grid[field_size-y][x-1] == -1 or enemy_grid[field_size-y][x-1] == 2:
			return -1

		plays[current_player].append([x-1, field_size-y])

		if enemy_grid[field_size-y][x-1] == 0:
			enemy_grid[field_size-y][x-1] = -1
			return 0
		else:
			enemy_grid[field_size-y][x-1] = 2
			hits[current_player] += 1
			is_horizontal = is_placed_horizontally(enemy_grid, x, y)
	
		direction = 1
		offset = direction
		found_ship_square = False
		while True:	# checks where the ship starts relative x and y
			if (is_horizontal and is_in_bounds(x-1-offset) and enemy_grid[field_size-y][x-1-offset] > 0) or (not is_horizontal and is_in_bounds(field_size-y-offset) and enemy_grid[field_size-y-offset][x-1] > 0):
				offset += direction
				found_ship_square = True
			else:
				if not found_ship_square:
					direction *= -1
					offset = direction
					found_ship_square = True
				else:
					offset -= direction
					break

		while True:
			if is_horizontal:
				if is_in_bounds(x-1-offset) and enemy_grid[field_size-y][x-1-offset] <= 0:
					break
				elif not is_in_bounds(x-1-offset):
					break
			else:
				if is_in_bounds(field_size-offset-y) and enemy_grid[field_size-y-offset][x-1] <= 0:
					break
				elif not is_in_bounds(field_size-offset-y):
					break

			if is_horizontal and (not is_in_bounds(x-1-offset) or not enemy_grid[field_size-y][x-1-offset] == 2) or not is_horizontal and (not is_in_bounds(field_size-y-offset) or not enemy_grid[field_size-y-offset][x-1] == 2):
				return 1
			else:
				offset -= direction

		return 2

##################################

	players = (p1, p2)
	wins = [0, 0]

	n_duels = 5
	for p in players:
		p.procedures.start(n_duels)

	for duel in range(n_duels):
		global hits, plays, ships, fields
		hits, plays, ships, fields = [0, 0], [[], []], [[], []], ([], [])
		for grid in fields:
			reset(grid, 0)

		first_player = duel%2
		print(f"\n\nRound {duel+1}!\nPlayer {first_player+1} goes first!")

		for i, p in enumerate(players):
			global current_player
			current_player = i
			p.procedures.place_all_ships(callbacks=[place])
			print_grid(fields[i])

		if not sorted(ships[current_player]) == [1, 1, 1, 1, 2, 2, 2, 3, 3, 4]:
			print(f"Player {current_player+1} did not place the correct ships, so Player {(current_player+1)%2+1} wins!\n")
			break

		for i, p in enumerate(players):
			current_player = i
			p.procedures.play(callbacks=[guess])

		print(f"\nPlayer 1's grid:      \t\tPlayer 2's grid:")
		print_grids()

		if hits[first_player] < hits[(first_player-1)%2] or hits[first_player] == hits[(first_player-1)%2] and len(plays[first_player]) > len(plays[(first_player-1)%2]):
			print(f"Player {(first_player-1)%2+1} sank all of Player {first_player+1}'s ships first!")
			wins[(first_player-1)%2] += 1
		else:
			print(f"Player {first_player+1} sank all of Player {(first_player-1)%2+1}'s ships first!")
			wins[first_player] += 1

print(f"Final score: {wins[0]}-{wins[1]}.", end=' ')
if wins[0] > wins[1]:
	print("Player 1 wins!")
elif wins[0] < wins[1]:
	print("Player 2 wins!")
else:
	print("It's a draw!")
