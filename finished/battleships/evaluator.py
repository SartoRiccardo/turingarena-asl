import random
from turingarena import *
try:
	from termcolor import colored
	tc = True
except ImportError:
	print("Install the termcolor module for a better experience.")
	tc = False

WIDTH = 10
HEIGHT = 10

SHIP_WAS_PLACED_CORRECTLY = 0
ERROR_IN_PLACING_SHIP = 1

ALREADY_GUESSED = -1
MISS, WATER = 0, 0
HIT, SHIP = 1, 1
HIT_AND_SANK, SANK = 2, 2

WON = 1
NO_RESULT = -1

board_pieces = {
	-1: ('+', 'blue'),
	0: ('~', 'cyan'),
	1: ('#', 'white'),
	2: ('/', 'red')
}

error = None
error_messages = {
	"guessed_twice": "Player {} tried to guess twice!",
	"out_of_range": "Player {} guessed a coordinate that's out of range!",
	"incorrect_ships": "Player {} placed some wrong ships!",
	"both_incorrect_ships": "Both players placed some wrong ships!"
}

def reset(matrix, value):
	global field_size
	field_size = 10
	for i in range(len(matrix)):
		matrix.pop(i)
	
	for i in range(field_size):
		row = [value for j in range(field_size)]
		matrix.append(row)

def print_fields():
	for i in range(len(fields)):
		print(f"Player {i+1}'s field:", end="     \t\t")
	print()

	for i in range(len(fields[0])):
		for f in fields:
			print("{:02d}".format(HEIGHT-i-1), end=' ')
			for j in range(len(f[i])):
				square = f[HEIGHT-i-1][j]
				if tc:
					print(colored(board_pieces[square][0], board_pieces[square][1]), end=' ')
				else:
					print(board_pieces[square][0], end=' ')

			print("\t\t", end='')
		print()

	for f in fields:
		print("   ", end='')
		for i in range(WIDTH):
			print(i, end=' ')
		print("\t\t", end='')

	print()

def force_win():
	global outcome
	outcome = WON

def check_ships():
	global current_p
	wrong_p = None
	for current_p, p_ships in enumerate(ships):
		ship_len = []
		for ship in p_ships:
			ship_len.append(len(ship))

		if not sorted(ship_len) == [1, 1, 1, 1, 2, 2, 2, 3, 3, 4]:
			if wrong_p == None:
				wrong_p = current_p
			else:
				wrong_p = 2

	global error
	if wrong_p == 2:
		error = "both_incorrect_ships"
		raise RuntimeError("Incorrect ships placed")
	elif not wrong_p == None:
		current_p = wrong_p	
		force_win()
		error = "incorrect_ships"
		raise RuntimeError("Incorrect ships placed")
	

def check_if_won():
	f = fields[(current_p+1)%2]
	for i in range(HEIGHT):
		for j in range(WIDTH):
			if f[i][j] == SHIP:
				return NO_RESULT
	return WON


MATCHES = 5
wins = [0, 0]
for game in range(MATCHES):
	fields = ([], [])
	ships = ([], [])
	moves = ([], [])
	for f in fields:
		reset(f, 0)

	with run_algorithm(submission.player1) as p1, run_algorithm(submission.player2) as p2:

		def place(ship_len, x, y, is_horizontal):	# callback: places the ship
			def in_bounds():	# checks if the whole ship is in bounds
				is_in = x >= 0 and y >= 0
				if is_horizontal:
					is_in = is_in and x+ship_len-1 < WIDTH and y < HEIGHT
				else:
					is_in = is_in and x < WIDTH and y+ship_len-1 < HEIGHT
				return is_in

			def overlaps():		# checks if it overlaps with/touches another ship
				if f[y][x] == SHIP:
					return True

				rel_coords = ([1, 0], [-1, 0], [0, 1], [0, -1])
				bkx, bky = x, y
				for i in range(ship_len):
					for rc in rel_coords:
						try:
							if f[bky+rc[0]][bkx+rc[1]] == SHIP:
								return True
						except IndexError:
							continue

					if is_horizontal:
						bkx += 1
					else:
						bky += 1

				return False

			y = HEIGHT-y-1
			f = fields[current_p]
			if not in_bounds() or overlaps():
				return ERROR_IN_PLACING_SHIP

			ship = []
			for i in range(ship_len):
				ship.append([y, x])
				f[y][x] = SHIP
				if is_horizontal:
					x += 1
				else:
					y += 1

			ships[current_p].append(ship)
			return SHIP_WAS_PLACED_CORRECTLY


		def guess(x, y):
			def in_range(x, y):
				return x >= 0 and x < WIDTH and y >= 0 and y < HEIGHT

			global error
			if not in_range(x, y):
				force_win()
				error = "out_of_range"
				raise RuntimeError("x, y or both out of range")
				
			global already_guessed
			if already_guessed:
				force_win()
				error = "guessed_twice"
				raise RuntimeError("Guessed twice")
			already_guessed = True

			y = HEIGHT-y-1
			coords = [y, x]
			
			if coords in moves[current_p]:
				return ALREADY_GUESSED
			moves[current_p].append(coords)

			f = fields[(current_p+1)%2]
			if f[y][x] == WATER:
				f[y][x] = ALREADY_GUESSED
				return MISS

			f[y][x] = SANK
			hit_ship = None
			for s in ships[(current_p+1)%2]:
				if coords in s:
					hit_ship = s
					break

			for square in hit_ship:
				if not square in moves[current_p]:
					return HIT

			return HIT_AND_SANK

		players = (p1, p2)

		for current_p, p in enumerate(players):
			p.procedures.place_all_ships(callbacks=[place])

		try:
			outcome = NO_RESULT
			check_ships()

			current_p = game%2
			print(f"Player {current_p+1} goes first!")
			current_p = (current_p+1) % 2
			while outcome < WON:
				current_p = (current_p+1) % 2
				already_guessed = False

				players[current_p].procedures.play_turn(callbacks=[guess])

				outcome = check_if_won()

		except RuntimeError:
			print(error_messages[error].format(current_p+1))
			current_p = (current_p+1) % 2

		print_fields()
		if not outcome == NO_RESULT:
			wins[current_p] += 1
			print(f"Player {current_p+1} wins game {game+1}!")
		else:
			print("It's a tie!")

		print()

print(f"Final score: {wins[0]} - {wins[1]}.", end = ' ')
if wins[0] > wins[1]:
	print(f"Player 1 wins the match!")
elif wins[0] < wins[1]:
	print(f"Player 2 wins the match!")
else:
	print("It's a tie!")






	
