#include <cstdlib>
#include <ctime>

// Constant declarations
#define WIDTH 10
#define HEIGHT 10
#define FOUR_PANEL_SHIPS 1
#define THREE_PANEL_SHIPS 2
#define TWO_PANEL_SHIPS 3
#define ONE_PANEL_SHIPS 4
#define SHIP_WAS_PLACED_CORRECTLY 0
#define ERROR_IN_PLACING_SHIP 1
#define ALREADY_GUESSED -1
#define MISS 0
#define HIT 1
#define HIT_AND_SANK 2

int x = WIDTH-1, y = -1;

// initialize random number generator
__attribute__((constructor)) void init() {
    srand(time(nullptr));
}

void place_all_ships(int place(int ship_len, int x, int y, int is_horizontal)) {
	int result;
    for(int i = 0; i < FOUR_PANEL_SHIPS; i++) {
		result = 0;
		do result = place(4, rand()%WIDTH, rand()%HEIGHT, 1);
		while(result != 0);
	}
	for(int i = 0; i < THREE_PANEL_SHIPS; i++) {
		result = 0;
		do result = place(3, rand()%WIDTH, rand()%HEIGHT, 1);
		while(result != 0);
	}
	for(int i = 0; i < TWO_PANEL_SHIPS; i++) {
		result = 0;
		do result = place(2, rand()%WIDTH, rand()%HEIGHT, 1);
		while(result != 0);
	}
	for(int i = 0; i < ONE_PANEL_SHIPS; i++) {
		result = 0;
		do result = place(1, rand()%WIDTH, rand()%HEIGHT, 1);
		while(result != 0);
	}
}

void play_turn(int guess(int x, int y)) {
	x++;
	if(x%10 == 0) {
		x=0; y++;
	}

	guess(x, y);
}
