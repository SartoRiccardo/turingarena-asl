import random

# Constant declarations
WIDTH = 10
HEIGHT = 10
FOUR_PANEL_SHIPS = 1
THREE_PANEL_SHIPS = 2
TWO_PANEL_SHIPS = 3
ONE_PANEL_SHIPS = 4
SHIP_WAS_PLACED_CORRECTLY = 0
ERROR_IN_PLACING_SHIP = 1
ALREADY_GUESSED = -1
MISS = 0
HIT = 1
HIT_AND_SANK = 2


def place_all_ships(place):
	for ship in range(4):
		place(ship, random.randint(0, WIDTH-1), random.randint(0, HEIGHT-1), random.randint(0, 1))

def play_turn(guess):
	x = random.randint(0, WIDTH-1)
	y = random.randint(0, HEIGHT-1)
	
	guess(x, y)
