import random

# Constant declarations
WIDTH = 10
HEIGHT = 10

FOUR_PANEL_SHIPS = 1
THREE_PANEL_SHIPS = 2
TWO_PANEL_SHIPS = 3
ONE_PANEL_SHIPS = 4

SHIP_WAS_PLACED_CORRECTLY = 0
ERROR_IN_PLACING_SHIP = 1

ALREADY_GUESSED = -1
MISS = 0
HIT = 1
HIT_AND_SANK = 2


def place_all_ships(place):
	ships = [FOUR_PANEL_SHIPS, THREE_PANEL_SHIPS, TWO_PANEL_SHIPS, ONE_PANEL_SHIPS]
	lengths = [4, 3, 2, 1]
	for i in range(len(lengths)):
		for j in range(ships[i]):
			result = ERROR_IN_PLACING_SHIP
			while not result == SHIP_WAS_PLACED_CORRECTLY:
				result = place(lengths[i], random.randint(0, WIDTH-1), random.randint(0, HEIGHT-1), random.randint(0, 1))

hits = []
def play_turn(guess):
	x = random.randint(0, WIDTH-1)
	y = random.randint(0, HEIGHT-1)

	while [x, y] in hits:
		x = random.randint(0, WIDTH-1)
		y = random.randint(0, HEIGHT-1)

	hits.append([x, y])
	guess(x, y)






	
