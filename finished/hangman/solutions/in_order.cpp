#define LETTER_NOT_GUESSED_YET -1

void play(int length, void guess(int letter), int get_word_letter(int i)) {

	for(int i = 97; i < 123; i++) {
		guess(i);

		for(int i = 0; i < length; i++) {
			if(get_word_letter(i) == -1) break;
			if(i == length-1) return;
		}
	}

}
