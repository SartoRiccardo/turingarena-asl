#define LETTER_NOT_GUESSED_YET -1

bool is_word_complete(int length, int *word) {

	for(int i = 0; i < length; i++) {
		if(word[i] == LETTER_NOT_GUESSED_YET) return false;
	}

	return true;

}

#include <stdlib.h>
void play(int length, void guess(int letter), int get_word_letter(int i)) {
	
	int *word = new int[length];
	for(int i = 0; i < length; i++)
		word[i] = LETTER_NOT_GUESSED_YET;

	while(!is_word_complete(length, word)) {
		int letter = rand() % 26 + 97;

		guess(letter);

		for(int i = 0; i < length; i++)
			word[i] = get_word_letter(i);
	}

}
