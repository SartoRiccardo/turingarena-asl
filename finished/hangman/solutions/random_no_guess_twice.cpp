#define LETTER_NOT_GUESSED_YET -1

bool is_word_complete(int length, int *word) {

	for(int i = 0; i < length; i++) {
		if(word[i] == LETTER_NOT_GUESSED_YET) return false;
	}

	return true;

}

bool is_in(int element, int *array, int array_len) {

	for(int i = 0; i < array_len; i++) {
		if(element == array[i])
			return true;
	}

	return false;

}

#include <stdlib.h>
void play(int length, void guess(int letter), int get_word_letter(int i)) {

	int guesses_len = 28;
	int *guesses = new int[guesses_len];
	int guesses_i = 0;	

	int *word = new int[length];
	for(int i = 0; i < length; i++)
		word[i] = LETTER_NOT_GUESSED_YET;

	while(!is_word_complete(length, word)) {
		int letter = LETTER_NOT_GUESSED_YET;
		do {
			letter = rand() % 26 + 97;
		} while(is_in(letter, guesses, guesses_len));
		
		guess(letter);
		guesses[guesses_i] = letter;
		guesses_i++;

		for(int i = 0; i < length; i++)
			word[i] = get_word_letter(i);
	}

	delete[] guesses;
	delete[] word;

}
