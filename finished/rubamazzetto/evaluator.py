from turingarena import *
from contextlib import ExitStack
import random

DECK_TOP_CARD_INDEX = 0
DECK_LENGTH_INDEX = 1
DECK_OWNER_INDEX = 2

DECK_STOLEN = 0
CANT_STEAL_DECK = 1

CARDS_IN_HAND = 3
CARDS_ON_FIELD = 4

error_msg = {
	"played_card_twice": "Player {} tried to play 2 cards in the same turn",
	"card_not_in_hand": "Player {} tried to play a card he didn't have",
	"not_played": "Player {} didn't even bother playing a card"
}

def get_sources():
	sources = []
	i = 0
	while True:
		try:
			i += 1
			sources.append(getattr(submission, f"player{i}"))
		except KeyError:
			break

	return sources

def give_hands():
	global hands, deck
	hands = [[deck[offset*CARDS_IN_HAND + i] for i in range(CARDS_IN_HAND)] for offset in range(len(players))]
	remove_cards(len(players) * CARDS_IN_HAND)
	return
	hands = []
	offset = 0
	for i in range(len(players)):
		p_hand = []
		for j in range(CARDS_IN_HAND):
			p_hand.append(deck[offset*CARDS_IN_HAND] + j)
		offset += 1
		hands.append(p_hand)
		

def remove_cards(num):
	global deck
	deck = deck[num:]

def play(p):
	def play_card(card):
		global played, error
		if played:
			error = "played_card_twice"
			raise RuntimeError(error)
		played = True

		global hands, field, p_decks
		cp_hand = hands[current_p]
		cp_deck = p_decks[current_p]

		if not card in cp_hand:
			error = "card_not_in_hand"
			raise RuntimeError(error)

		cp_hand.remove(card)
		for cardf in field:
			if card == cardf:
				field.remove(card)
				cp_deck[DECK_LENGTH_INDEX] += 2
				cp_deck[DECK_TOP_CARD_INDEX] = card
				return

		field.append(card)


	def steal_deck(deck_owner):
		def steal():
			cp_hand.remove(card)
			cp_deck[DECK_LENGTH_INDEX] += ep_deck[DECK_LENGTH_INDEX]+1
			ep_deck[DECK_LENGTH_INDEX] = 0
			ep_deck[DECK_TOP_CARD_INDEX] = 0

		global played, error
		if played:
			error = "played_card_twice"
			raise RuntimeError(error)

		global hands, p_decks
		cp_hand = hands[current_p]
		ep_deck, cp_deck = p_decks[deck_owner], p_decks[current_p]

		for card in cp_hand:
			if card == ep_deck[DECK_TOP_CARD_INDEX]:
				steal()
				played = True
				return DECK_STOLEN

		return CANT_STEAL_DECK


	global played, error
	p = players[current_p]
	p.procedures.start_turn(len(hands[current_p]), len(field), len(players)-1)

	played = False
	p.procedures.play_turn(hands[current_p], field, p_decks[:current_p]+p_decks[current_p+1:], callbacks=[play_card, steal_deck])

	if not played:
		error = "not_played"
		raise RuntimeError(error)

def print_decks():
	lengths = [d[DECK_LENGTH_INDEX] for d in p_decks]
	max_len = sorted(lengths)[-1]

	for i in range(max_len+1):
		for d in p_decks:
			if max_len-d[DECK_LENGTH_INDEX]+1 <= i:
				print('__', end='\t')
			elif max_len-i == d[DECK_LENGTH_INDEX]:
				print('{:02d}'.format(d[DECK_LENGTH_INDEX]), end='\t')
			else:
				print('  ', end='\t')
		print()

	for i in range(len(players)):
		print(f"P{i+1}", end='\t')
	print()



allowed_players = (2, 3, 4, 6, 12)
sources = get_sources()
if not len(sources) in allowed_players:
	raise RuntimeError("You can only have 2, 3, 4, 6 or 12 players.")

wins = [0 for i in range(len(sources))]
GAMES = len(sources)*3
for game in range(GAMES):
	deck = [i+1 for i in range(10)] * 4
	random.shuffle(deck)

	with ExitStack() as stack:
		players = [stack.enter_context(run_algorithm(source)) for source in sources]

		p_decks = [[0, 0, i] for i in range(len(players))]
		field = deck[:CARDS_ON_FIELD]
		remove_cards(CARDS_ON_FIELD)

		current_p = game % len(players)
		error = None
		print(f"Player {current_p+1} goes first!")


		dq_players = []
		while len(deck) > 0:
			give_hands()

			for i in range(CARDS_IN_HAND):
				for j in range(len(players)):
					try:
						if current_p not in dq_players:
							play(players[current_p])
						
					except RuntimeError:
						print(error_msg[error].format(current_p+1) + ", therefore he gets eliminated!")
						dq_players.append(current_p)

					current_p = (current_p+1) % len(players)


		winners = [0]
		for i in range(1, len(p_decks)):
			if p_decks[i][DECK_LENGTH_INDEX] > p_decks[winners[0]][DECK_LENGTH_INDEX]:
				winners = [i]
			elif p_decks[i][DECK_LENGTH_INDEX] == p_decks[winners[0]][DECK_LENGTH_INDEX]:
				winners.append(i)

		print_decks()
		if len(winners) == 1:
			print(f'Player {winners[0]+1} wins game {game+1}!')
			wins[winners[0]] += 1
		else:
			print('Players', end=' ')
			for i in range(len(winners)):
				print(f"{winners[i]+1}", end=' ')
				wins[winners[i]] += 1
				if i == len(winners)-2:
					print('and', end=' ')
			print(f'win game {game+1}!')
		print()

print("Final score:")
for i in range(len(wins)):
	print(f"Player {i+1}: {wins[i]}")

winners = [0]
for i in range(1, len(p_decks)):
	if wins[i] > wins[winners[0]]:
		winners = [i]
	elif wins[i] == wins[winners[0]]:
		winners.append(i)

if len(winners) == 1:
	print(f"Player {winners[0]+1} wins the match!")
else:
	print('It\'s a tie between Players', end=' ')
	for i in range(len(winners)):
		print(f"{winners[i]+1}", end=' ')
		if i == len(winners)-2:
			print('and', end=' ')
print()

				
				







