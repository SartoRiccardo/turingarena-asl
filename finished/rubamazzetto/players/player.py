# Constant declarations
DECK_TOP_CARD_INDEX = 0
DECK_LENGTH_INDEX = 1
DECK_OWNER_INDEX = 2

DECK_STOLEN = 0
CANT_STEAL_DECK = 1

def start_turn(hs, fs, np):
	pass

def play_turn(hand, field, decks, play_card, steal_deck):
	for d in decks:
		for card in hand:
			if card == d[DECK_TOP_CARD_INDEX]:
				steal_deck(d[DECK_OWNER_INDEX])
				return

	for cardf in field:
		for cardh in hand:
			if cardh == cardf:
				play_card(cardh)
				return

	play_card(hand[0])
