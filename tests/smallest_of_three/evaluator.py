import random
from turingarena import *

all_correct = True
for _ in range(10):
	value_range = range(10 ** 3, 5 * 10 ** 3)
	a,b,c = random.choices(value_range, k=3)
	
	try:
		with run_algorithm(submission.source) as process:
			smallest = process.functions.smallest_of_three(a, b, c)

		correct_smallest = a
		if correct_smallest > b:
			correct_smallest = b
		if correct_smallest > c:
			correct_smallest = c

		if correct_smallest == smallest:
			print(f"The smallest between {a} {b} and {c} --> {smallest} (correct)")
		else:
			print(f"The smallest between {a} {b} and {c} --> {smallest} (wrong)")
			all_correct = False

	except AlgorithmError as e:
		print(f"The smallest between {a} {b} and {c} is {e} (wrong)")
		all_correct = False


evaluation.data(dict(goals=dict(correct=all_correct)))
