
int smallest_of_three(int a, int b, int c) {
    
 	int smallest = a;
	if(smallest > b) smallest = b;
	if(smallest > c) smallest = c;

	return smallest;

}

/*
#include <iostream>
using namespace std;
main() {

	int a, b, c;
	cin >> a;
	cin >> b;
	cin >> c;

	int smallest = smallest_of_three(a, b, c);
	cout << smallest << "\n";

}
*/
