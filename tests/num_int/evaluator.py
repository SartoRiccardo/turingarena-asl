import random
import math
from turingarena import *

def get_digit(num, pos):
	digit = math.floor(num / 10 ** pos) % 10
	return digit

def correct_opposite_number(num):
	opposite = 0

	for i in range(len(str(num))):
		opposite *= 10
		opposite += get_digit(num, i)

	return opposite

def correct_valuable_digit(num):
	max_digit = 0

	for i in range(len(str(num))):
		digit = get_digit(num, i)
		if digit > max_digit:
			max_digit = digit

	return max_digit


first_all_correct = True
last_all_correct = True
opposite_all_correct = True
valuable_digit_all_correct = True

for num in [0, 6, 10, 23, 33, 100, 101, 110 , 111, 432]:
	#num = random.randint(0, 200)
	
	try:
		with run_algorithm(submission.source) as process:
			last = process.functions.get_last_digit(num)
			first = process.functions.get_first_digit(num)
			opposite = process.functions.get_opposite_number(num)
			max_digit = process.functions.get_most_valuable_digit(num)

		print(f"The number {num}:")

		print(f"\tends with {last}", end=' ')
		if num%10 == last:
			print("(correct)")
		else:
			print("(wrong)")
			last_all_correct = False

		print(f"\tstarts with {first}", end=' ')
		if int( str(num)[0] ) == first:
			print("(correct)")
		else:
			print("(wrong)")
			first_all_correct = False

		print(f"\t's opposite number is {opposite}", end=' ')
		if correct_opposite_number(num) == opposite:
			print("(correct)")
		else:
			print("(wrong)")
			opposite_all_correct = False

		print(f"\t's most valuable digit is {max_digit}", end=' ')
		if correct_valuable_digit(num) == max_digit:
			print("(correct)")
		else:
			print("(wrong)")
			valuable_digit_all_correct = False

		print()

	except AlgorithmError as e:
		pass

evaluation.data(dict(goals=[dict(first_correct=first_all_correct), dict(last_correct=last_all_correct), dict(opposite_number_correct=opposite_all_correct), dict(valuable_digit_correct=valuable_digit_all_correct)]))
		
