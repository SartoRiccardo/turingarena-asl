from turingarena import *

for i in range(10):

	with run_algorithm(submission.source) as process:
		def my_callback(number, another_number):
			return number * another_number

		ans = process.functions.my_function(i, callbacks=[my_callback])

	print(ans)
