int *fibonacci_sequence;

void get_fibonacci_sequence(int n) {
	
	fibonacci_sequence = new int[n];
	
	if(n > 0) fibonacci_sequence[0] = 1;
	if(n > 1) fibonacci_sequence[1] = 1;

	for(int i = 2; i < n; i++) fibonacci_sequence[i] = fibonacci_sequence[i-2] + fibonacci_sequence[i-1];

}

int get_fibonacci_element(int i) {
	
	return fibonacci_sequence[i];

}

/*
#include <iostream>
using namespace std;
int main() {

	int n;
	cin >> n;
	get_fibonacci_sequence(n);
	
	for(int i = 0; i < n; i++)
		cout << get_fibonacci_element(i) << " ";

	cout << "\n";

}
*/
