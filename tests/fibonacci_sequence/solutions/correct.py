
def get_fibonacci_sequence(n, fibonacci_sequence):
	global a
	a = [1, 1]

	for i in range(2, n):
		a.append(a[i-2] + a[i-1])

	a = a[:n]

def get_fibonacci_element(i):
	return a[i]

def main():
	get_fibonacci_sequence(10, a)
	for i in range(len(a)):
		print(get_fibonacci_element(i))


main()
