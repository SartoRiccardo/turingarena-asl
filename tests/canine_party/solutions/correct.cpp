
int get_party_value(int n, int *friends, int is_funny(int dog_friend)) {
    
	int party_value = 0;

	for(int i = 0; i < n; i++) {

		if(is_funny(friends[i]) == 1) party_value += friends[i];	

	}

	return party_value;

}
