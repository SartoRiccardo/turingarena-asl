import random
from turingarena import *

all_passed = True
for _ in range(10):
	n = random.randint(3, 10)
	friends = [random.randint(-100, 100) for i in range(n)]

	with run_algorithm(submission.source) as process:
		def is_funny(friend):
			if(friend > 0):
				return 1
			else:
				return 0

		ans = process.functions.get_party_value(n, friends, callbacks=[is_funny])

	print(ans)
