import random
from turingarena import *

def correct_smallest_of_n(numbers):
	smallest = numbers[0]

	for i in range(len(numbers)):
		if smallest > numbers[i]:
			smallest = numbers[i]

	return smallest;

all_correct = True
for _ in range(10):
	value_range = range(10 ** 3, 5 * 10 ** 3)
	numbers = random.sample(value_range, k=random.randint(1, 15))

	try:
		with run_algorithm(submission.source) as process:
			smallest = process.functions.smallest_of_n(len(numbers), numbers)

		if correct_smallest_of_n(numbers) == smallest:
			print(f"(correct) The smallest number in {numbers} is {smallest}")
		else:
			print(f"(wrong) The smallest number in {numbers} is {smallest}")
			all_correct = False


	except AlgorithmError as e:
			print(f"(error) The smallest number in {numbers} is {e}")
			all_correct = False


evaluation.data(dict(goals=dict(correct=all_correct)))
