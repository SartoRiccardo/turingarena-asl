int smallest_of_n(int n, int *numbers) {

	int smallest = numbers[0];

	for(int i = 0; i < n; i++) {

		if(smallest > numbers[i]) smallest = numbers[i];

	}

	return smallest;

}

/*
#include <iostream>
using namespace std;
int main() {

	int len;
	cin >> len;
	int num[len];

	for(int i = 0; i < len; i++) cin >> num[i];

	cout << smallest_of_n(len, num);

}
*/
