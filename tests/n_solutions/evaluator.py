from turingarena import *
from contextlib import ExitStack

sources = []
get_more_sources_cmd = "sources.append(submission.player{})"
i = 0
while True:
	try:
		i += 1
		exec(get_more_sources_cmd.format(i))
	except KeyError:
		break

with ExitStack() as stack:
	players = tuple([stack.enter_context(run_algorithm(source)) for source in sources])

	a, b = 10, 20

	for i, p in enumerate(players):
		if p.functions.sum(a, b) == a+b:
			print(f"Player {i+1} passes the test!")
		else:
			print(f"Player {i+1} gets kicked out!")
